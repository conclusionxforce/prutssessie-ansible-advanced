Exercise 2: Step by step Solution
=================================

* Define a list of the vhosts you want to configure, call this 'vhosts'
  You can do this directly in your playbook, or use a separate yml file

* You can use the ansible module 'blockinfile' to add the configuration 
  for each vhost to the apache configuration. Use a new .conf file in 
  /etc/httpd/conf.d/ .Example of block:
    <VirtualHost: *:8080>
        ServerName: wordpress.xforce.nl
        DocumentRoot: /var/www/NL/
    </VirtualHost>

* Loop this action using ' loop: "{{ vhosts }}" ' to add all vhosts in the list

* Make sure you use a different marker for each vhost (containing the name
  of the vhost: {{ item }} ), so the next blockinfile will not overwrite the 
  previous one

* Do not forget to restart the httpd service

 

