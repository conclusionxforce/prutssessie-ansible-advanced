Exercise 2: Configure Virtual Hosts
=====================

Needed:
* control node
* web1
* web2

Preparations:
* Exercise 1 completed

Goal:
The webservers should be reachable on 3 different URLs, and only on these.
wordpress.xforce.nl
wordpress.xforce.com
wordpress.xforce.org
Write a playbook to configure apache on the webservers to accomplish this.
Bonus: include tasks in this playbook to test whether the webservers can 
be reached on these URLs, and not on a random URL or their IP

Hints:
* https://httpd.apache.org/docs/2.4/
* To make a server unreachable, define a default virtual host

Are you stuck?
* In Lab/README.md the exercise is written out in steps
* In Solutions you can find a working playbook

