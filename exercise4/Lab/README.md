Exercise 4: Step by step Solution
=================================

* First remove the old configuration (vhosts etc) from the webservers

* Make te webservers listen on port 5000. Optionally make them accessible only from
  the loadbalancer

* Make sure the port is accessible for apache, by setting the correct SELinux context,
  and open up the port in the firewall 

* If you want to easily see which server is being used, make them display their IP 
  in index.html 

* Don't forget to (re)start apache

---------------------------------

* On the loadbalancer, install 'haproxy'

* Check the /etc/haproxy/haproxy.cfg file

* Modify the config (use either 'replace', 'copy', 'template',..etc) to listen on port
  80 and use roundrobin loadbalancing on the to webservers, port 5000

* Open up the firewall for http trafic on port 80

* Do not forget to (re)start the haproxyservice

 

