Oefening 0: Access
=====================

De volgende zaken moeten nog worden voorbereid voordat we echt goed met
Ansible aan de slag kunnen: 

* met SSH inloggen als centos@$ip op controlnode
* een SSH private/public keypair genereren
* public key verspreiden naar de clientnodes, user ansible
* Je kunt nu als ansible@$clientnode zonder wachtwoord inloggen
* Je kunt vervolgens op de clientnode sudo doen zonder wachtwoord

Control Node : user:centos 	pw:ubunturulez
Clients :      user:ansible	pw:puppetrulez
