Exercise 5: Reboot webfarm
=====================

Needed:
* control node
* web1
* web2
* proxy1

Preparations:
* Exercise 4 completed

Goal:
Even Linux webservers need to be rebooted every once in a while. Pretend there an
important update has to be installed and the servers need to be rebooted
Write a playbook that reboots the webservers, but makes sure the service always
stays available.

Hints:
* wait_for

Are you stuck?
* In Lab/README.md the exercise is written out in steps
* In Solutions you can find a working playbook

