Aanwijzingen voor oefening 1
============================

WordPress heeft een webserver met PHP nodig, en een MySQL database. Nog even
voor de duidelijkheid: al deze stappen kun je en moet je dus ook met Ansible
doen :)

* Installeer op de webservers Apache HTTPD en PHP
* Voor WordPress is ook de php-mysql module nodig
* Zet deze service aan en start hem
* Omdat de MySQL server over het netwerk bereikbaar moet zijn moet je op de webservers de juiste SELinux boolean aanzetten
* Installeer op de databaseserver MariaDB Server
* Om via Ansible management te kunnen doen is er ook MySQL-python nodig
* Zet de MariaDB service aan en start hem
* Maak een MySQL user en database aan en wijs privileges toe. Let erop dat de database over het netwerk aangesproken gaat worden (hosts)
* WordPress is te downloaden vanaf https://wordpress.org/latest.tar.gz
* Pak deze tar.gz uit, bijvoorbeeld naar de standaard public root directory van Apache (/var/www/html)
* Configureer WordPress met behulp van een template, een voorbeeld wp-config.php staat in de Lab directory
* Als je het helemaal netjes wilt doen, kun je random salts voor WordPress laten genereren via https://api.wordpress.org/secret-key/1.1/salt/ en deze in je config bestand laten zetten

Extra punten om op te letten:
* Je configureert de MySQL database en de WordPress configuratie met dezelfde database naam, username, en servername. Je kunt hier dus variabelen definieren die je deelt onder deze plays
* Je configureert ook een MySQL password, deze moet via een Ansible Vault versleuteld worden zodat deze niet plain-text op een filesystem of in Git staat
* Ansible Modules die je vast wilt gebruiken in deze opdracht:
  - yum
  - service
  - seboolean
  - mysql_db
  - mysql_user
  - get_url
  - unarchive
  - uri
  - template
